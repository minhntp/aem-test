$(function() {

	var urlString = window.location.href;
	var url = new URL(urlString);
	var id = url.searchParams.get("id");

	console.log("Showing user with id: " + id);

	var endpoint = 'https://reqres.in/api/users/' + id;
	console.log("endpoint: " + endpoint);

	$.ajax({
		url: endpoint,
		contentType: "application/json",
		dataType: 'json',
		success: function(result) {
			console.log(result);

			$('#avatar').attr('src', result.data.avatar)
			$('#avatar').attr('alt', result.data.first_name);
			$('#firstName').attr('value', result.data.first_name);
			$('#lastName').attr('value', result.data.last_name);
			$('#email').attr('value', result.data.email);

			return result;
		}
	});

	$(document).on('click', 'button.page-link', function() {
		var page = $(this).data('page');
		console.log('Page clicked: ' + page);
		getUsers(page);
	});

});