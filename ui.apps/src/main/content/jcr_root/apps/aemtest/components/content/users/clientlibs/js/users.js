$(function() {
	console.log('Search New');

	getUsers(1);

	function getUsers(page) {
		console.log('loading page: ' + page);
		var endpoint = 'https://reqres.in/api/users?page=' + page;
		$.ajax({
			url: endpoint,
			contentType: "application/json",
			dataType: 'json',
			success: function(result) {
				console.log(result);
				var userTableBody = $('#user-list tbody');
				// Reset table body
				$(userTableBody).html('');
				
				var no = 1;
				$.each(result.data, function(index, user) {
					userTableBody.append(`
                    <tr>
                        <td>${no++}</td>
                        <td>${user.first_name}</td>
                        <td>${user.last_name}</td>
                        <td>${user.email}</td>
                        <td><button class="btn btn-info btn-sm user-link" 
						data-id="${user.id}">View</button></td>
                      </tr>`);
					console.log("User at: " + index + " ---- Email: " + user.email)
				});

				var pagination = $('ul.pagination');
				// Reset pagination
				$(pagination).html('');
				for (var i = 1; i <= result.total_pages; i++) {
					pagination.append(`<li class="page-item ${i === page ? "active" : ""}"><button class="page-link" data-page="${i}">${i}</button></li>`)
				}

				return result;
			}
		});
	}

	$(document).on('click', 'button.page-link', function() {
		var page = $(this).data('page');
		console.log('Page clicked: ' + page);
		getUsers(page);
	});

	$(document).on('click', 'button.user-link', function() {
		var id = $(this).data('id');
		console.log('User id clicked: ' + id);
		window.location.href = '/content/aemtest/us/en/users/user.html?id=' + id;
	});



});