$(function () {
    console.log('Form Submit');
    var forms = document.querySelectorAll('.user-form.needs-validation');
    var email = document.getElementsByName("email")[0];
	var firstName = document.getElementsByName("firstName")[0];
	var lastName = document.getElementsByName("lastName")[0];

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
        .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                console.log('onSubmit');
                if (!form.checkValidity()) {
                    event.preventDefault();
                    event.stopPropagation();

                    if (!email.validity.valid) {
                        console.log('Invalid email');
                        var inputContainer = $('[name="email"]').closest("div.text-input");
                        if (email.validity.valueMissing) {
                            inputContainer.find('.invalid-feedback').html(inputContainer.data('required-msg'));
                        } else if (email.validity.patternMismatch) {
                            console.log('Invalid email address!');
                            var errorMessage = inputContainer.data('regex-msg');
                            if (!errorMessage) {
                                errorMessage = "Please enter a valid email address";
                            }
                            inputContainer.find('.invalid-feedback').html(errorMessage);
                        }
                    }

					if (!firstName.validity.valid) {
						console.log('Invalid first name');
						var inputContainer = $('[name="firstName"]').closest("div.text-input");
						if (firstName.validity.valueMissing) {
							inputContainer.find('.invalid-feedback').html(inputContainer.data('required-msg'));
						} else if (firstName.validity.patternMismatch) {
							console.log('Inavlid first name');
							var errorMessage = inputContainer.data('regex-msg');
							if (!errorMessage) {
								errorMessage = "Please enter a valid First Name";
							}
							inputContainer.find('.invalid-feedback').html(errorMessage);
						}
					}

					if (!lastName.validity.valid) {
						console.log('Invalid last name');
						var inputContainer = $('[name="lastName"]').closest("div.text-input");
						if (lastName.validity.valueMissing) {
							inputContainer.find('.invalid-feedback').html(inputContainer.data('required-msg'));
						} else if (lastName.validity.patternMismatch) {
							console.log('Inavlid last name');
							var errorMessage = inputContainer.data('regex-msg');
							if (!errorMessage) {
								errorMessage = "Please enter a valid Last Name";
							}
							inputContainer.find('.invalid-feedback').html(errorMessage);
						}
					}
                }

                form.classList.add('was-validated')
            }, false)
        })
});