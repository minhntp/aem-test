package com.scott.core.configs;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(
		name = "Send Email Configuration",
		description = "This configuration set the email")
public @interface SendEmailConfiguration {

	@AttributeDefinition(
			name = "Email Address",
			description = "Email address")
	String getEmailAddress();

	@AttributeDefinition(
			name = "Subject",
			description = "Email Subject")
	String getSubject();
}
