package com.scott.core.configs.impl;

import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.Designate;
import com.scott.core.configs.SendEmailConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(
		service = SendEmailConfigurationImpl.class,
		immediate = true,
		property = {
				Constants.SERVICE_ID + "=Send Email Service",
				Constants.SERVICE_DESCRIPTION + "=This service reads values from Send Email Configuration"
		})
@Designate(ocd = SendEmailConfiguration.class)
public class SendEmailConfigurationImpl {

	private static final String TAG = SendEmailConfigurationImpl.class.getSimpleName();
	private static final Logger LOGGER = LoggerFactory.getLogger(SendEmailConfigurationImpl.class);

	private String emailAddress;
	private String subject;

	@Activate
	protected void activate(SendEmailConfiguration configuration) {
		this.emailAddress = configuration.getEmailAddress();
		LOGGER.info("Email Address: {}", this.emailAddress);
		this.subject = configuration.getSubject();
		LOGGER.info("Subject: {}", this.subject);
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public String getSubject() {
		return this.subject;
	}
}
