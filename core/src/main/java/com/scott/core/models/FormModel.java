package com.scott.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FormModel {

	/** Resource instance.*/
	@SlingObject
	private Resource resource;

	/**
	 * Returns the generated hashcode.
	 * @return int - numeric hashcode generated
	 */
	public final int getHashCode() {
		return resource.getPath().hashCode();
	}
}
