package com.scott.core.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.mailer.MessageGateway;
import com.day.cq.mailer.MessageGatewayService;
import com.scott.core.configs.SendEmailConfiguration;
import com.scott.core.configs.impl.SendEmailConfigurationImpl;

@Component(service = Servlet.class,
		property = {
				Constants.SERVICE_DESCRIPTION + "=AEM Test - Save summitted user info Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_POST,
				"sling.servlet.paths=" + "/bin/api/saveuser"
		})
public class SaveUserServlet extends SlingAllMethodsServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = -795417251011587421L;
	private static final String TAG = SaveUserServlet.class.getSimpleName();
	private static final Logger LOGGER = LoggerFactory.getLogger(SaveUserServlet.class);

	@Reference
	SendEmailConfigurationImpl sendEmailConfiguration;

	@Reference
	MessageGatewayService messageGatewayService;

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {

		LOGGER.info("{}: getting user info to save...", TAG);

		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");
		String gender = request.getParameter("gender");
		String remarks = request.getParameter("remarks");

		User user = new User(firstName, lastName, email, gender, remarks);

		String receivedInfo = user.toString();

		LOGGER.info("{}: received info: {}", TAG, receivedInfo);

		// save to session
		HttpSession session = request.getSession();
		session.setAttribute("user", user);
		session.setAttribute("test", "test content");

		// send email
		try {

			sendEmail(email, receivedInfo);

		} catch (Exception e) {
			LOGGER.info("{}: error while trying to send email: {}", TAG, e.getMessage());
		}

		// go to result page to show submitted information
		response.sendRedirect("/bin/api/showsaveduser");

	}

	// ------------------------------------------------------------------------------------------------

	class User implements Serializable {

		/**
		 *
		 */
		private static final long serialVersionUID = 6914950187310240439L;
		String firstName;
		String lastName;
		String email;
		String gender;
		String remarks;

		public User(String firstName, String lastName, String email, String gender, String remarks) {
			this.firstName = firstName;
			this.lastName = lastName;
			this.email = email;
			this.gender = gender;
			this.remarks = remarks;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getGender() {
			return gender;
		}

		public void setGender(String gender) {
			this.gender = gender;
		}

		public String getRemarks() {
			return remarks;
		}

		public void setRemarks(String remarks) {
			this.remarks = remarks;
		}

		@Override
		public String toString() {
			return "User {firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", gender=" + gender
					+ ", remarks=" + remarks + "}";
		}

	}

	private void sendEmail(String from, String content) {

		MessageGateway<Email> messageGateway;

		Email email = new SimpleEmail();

		try {

			email.addTo(sendEmailConfiguration.getEmailAddress());

			email.setSubject(sendEmailConfiguration.getSubject());

			email.setFrom(from);

			email.setMsg(content);

			//Inject a MessageGateway Service and send the message

			messageGateway = messageGatewayService.getGateway(Email.class);

			LOGGER.info("{}: sending email...", TAG);

			// Check the logs to see that messageGateway is not null

			messageGateway.send(email);

			LOGGER.info("{}: Email sent!", TAG);

		} catch (Exception e) {

			StringBuilder errorMessage = new StringBuilder();

			errorMessage.append(e.getCause().getLocalizedMessage())
					.append("\n")
					.append(e.getCause().getMessage())
					.append("\n")
					.append(e.getCause().getStackTrace())
					.append("\n")
					.append(e.getLocalizedMessage())
					.append("\n")
					.append(e.getMessage())
					.append("\n")
					.append(e.getStackTrace());

			LOGGER.info("{}: exception sending email: {}", TAG, errorMessage);

		}
	}

}
