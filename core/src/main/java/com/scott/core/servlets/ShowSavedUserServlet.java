package com.scott.core.servlets;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scott.core.servlets.SaveUserServlet.User;

@Component(service = Servlet.class,
		property = {
				Constants.SERVICE_DESCRIPTION + "=AEM Test - Show saved user info in session Servlet",
				"sling.servlet.methods=" + HttpConstants.METHOD_GET,
				"sling.servlet.paths=" + "/bin/api/showsaveduser"
		})
public class ShowSavedUserServlet extends SlingSafeMethodsServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = -2845767803857832128L;
	private static final String TAG = ShowSavedUserServlet.class.getSimpleName();
	private static final Logger LOGGER = LoggerFactory.getLogger(ShowSavedUserServlet.class);

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		User user = (User) session.getAttribute("user");

		response.getWriter().println("Submitted user:\n\n" + user);

	}
}
